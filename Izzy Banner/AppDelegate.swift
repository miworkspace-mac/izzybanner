//
//  AppDelegate.swift
//  Izzy Banner
//
//  Created by Jim Zajkowski on 7/10/15.
//  Copyright © 2015 University of Michigan. All rights reserved.
//

import Cocoa

func centerRectInRect(rectToCenter rectToCenter: NSRect, toRect: NSRect) -> NSRect
{
    return NSOffsetRect(rectToCenter,
        NSMidX(toRect) - NSMidX(rectToCenter),
        NSMidY(toRect) - NSMidY(rectToCenter) )
}

let EDGEINSET = 16

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    var largeTextWindow: NSWindow!
    let carbonHelpers = CarbonHelpers()

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        
        carbonHelpers.wakeUpScreen()
        
        var stringToShow: String = "Eat at Joe's"
        var backColor: NSColor = NSColor.darkGrayColor()
        let textColor = NSColor.whiteColor()
        
        if let message = NSUserDefaults.standardUserDefaults().stringForKey("message")
        {
            stringToShow = message
        }
        
        switch NSUserDefaults.standardUserDefaults().stringForKey("color")
        {
        case .Some("green"):
            backColor = NSColor.greenColor()
        
        case .Some("red"):
            backColor = NSColor.redColor()
        
        case .Some("yellow"):
            backColor = NSColor.yellowColor()

        default:
            backColor = NSColor.darkGrayColor()
        }
        
        
        let screenRect = NSScreen.mainScreen()!.frame
        let displayWidth = NSWidth(screenRect) * 11.0/12.0 - CGFloat(2 * EDGEINSET)
        let displayHeight = NSHeight(screenRect) * 11.0/12.0 - CGFloat(2 * EDGEINSET)
        
        var computedSize = 40
        
        for size in 24...300 {
            computedSize = size
            // Test the *next* size up - we break when we that size is just too big
            let textFont = NSFont.boldSystemFontOfSize(CGFloat(size) + 1.0);
            let textSize = stringToShow.sizeWithAttributes([NSFontAttributeName: textFont])
            
            if ((textSize.width > (displayWidth - 50) ) ||
                (textSize.height) > (displayHeight + textFont.descender + textFont.ascender) )
            {
                NSLog("our: %d", size)
                break
            }
        }
        
        let formattedString = NSMutableAttributedString(string: stringToShow)
        let fullRange = NSMakeRange(0, formattedString.length)

        // Centering
        let paragraphStyle: NSMutableParagraphStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.alignment = NSCenterTextAlignment
        formattedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: fullRange)
        
        // Font
        formattedString.addAttribute(NSFontAttributeName, value: NSFont.boldSystemFontOfSize(CGFloat(computedSize)), range: fullRange)
        
        // Color
        formattedString.addAttribute(NSForegroundColorAttributeName, value: textColor, range: fullRange)
        
        // Shadow
        let textShadow = NSShadow()
        textShadow.shadowOffset = NSMakeSize(5, -5)
        textShadow.shadowBlurRadius = 10.0
        textShadow.shadowColor = NSColor(deviceWhite: 0, alpha: 0.64)
        formattedString.addAttribute(NSShadowAttributeName, value: textShadow, range: fullRange)
        
        // Make text view
        let textView = NSTextView(frame: NSMakeRect(0, 0, displayWidth, 0))
        textView.editable = false
        textView.selectable = false
        textView.drawsBackground = false
        textView.textStorage?.setAttributedString(formattedString)
        textView.sizeToFit()
        
        // Compute height of the view
        var textFrame = textView.frame
        let layoutManager = textView.layoutManager
        var numberOfLines = 0
        var index = 0
        var requiredHeight: CGFloat = 0.0
        let numberOfGlyphs = layoutManager?.numberOfGlyphs
        
        while (index < numberOfGlyphs) {
            var lineRange: NSRange = NSMakeRange(0, 0)
            
            if let rect = layoutManager?.lineFragmentRectForGlyphAtIndex(index, effectiveRange: &lineRange) {
                requiredHeight += NSHeight(rect)
                index = NSMaxRange(lineRange)
                numberOfLines++
            }
        }
        textFrame.size.height = CGFloat(numberOfLines) * (layoutManager?.defaultLineHeightForFont(NSFont.boldSystemFontOfSize(CGFloat(computedSize))))!
        textFrame.size.height = [NSHeight(screenRect) - 80.0, NSHeight(textFrame)].minElement()!
        textView.frame = textFrame
        textView.backgroundColor = NSColor.blueColor()
        
        var windowRect = centerRectInRect(rectToCenter: textFrame, toRect: screenRect)
        windowRect = NSInsetRect(windowRect, -10, -10)
        windowRect = NSIntegralRect(windowRect)
        
        largeTextWindow = LargeTextEphemeralPanel(
            contentRect: windowRect,
            styleMask: NSBorderlessWindowMask,
            backing: .Buffered,
            `defer`: false,
            screen: NSScreen.mainScreen()
        )

        largeTextWindow.setFrame(centerRectInRect(rectToCenter: windowRect, toRect: screenRect), display: true)
        largeTextWindow.ignoresMouseEvents = false
        largeTextWindow.backgroundColor = NSColor.clearColor()
        largeTextWindow.opaque = false
        largeTextWindow.level = 10000
        largeTextWindow.hidesOnDeactivate = false
        largeTextWindow.hasShadow = true
        
        let coloredRoundRectView = ColoredRoundedRectView()
        coloredRoundRectView.backColor = backColor
        coloredRoundRectView.addSubview(textView)

        largeTextWindow.contentView = coloredRoundRectView

        // views should be ready now
        textView.frame = centerRectInRect(rectToCenter: textView.frame, toRect: coloredRoundRectView.frame)
        largeTextWindow.initialFirstResponder = textView
        largeTextWindow.contentView.display()
        largeTextWindow.canBecomeVisibleWithoutLogin = true
        largeTextWindow.orderFrontRegardless()
        largeTextWindow.makeKeyAndOrderFront(nil)
        carbonHelpers.hideMenubar()
        
    }

    // Shut down on keydown
    func applicationShouldTerminateAfterLastWindowClosed(sender: NSApplication) -> Bool {
        return true
    }
}

