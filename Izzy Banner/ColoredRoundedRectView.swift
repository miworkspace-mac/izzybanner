//
//  ColoredRoundedRectView.swift
//  Izzy Banner
//
//  Created by Jim Zajkowski on 7/10/15.
//  Copyright © 2015 University of Michigan. All rights reserved.
//

import Cocoa

class ColoredRoundedRectView: NSView {
    
    var backColor: NSColor = NSColor.blueColor()
    
    override var opaque: Bool {
        get {
            return false
        }
    }
    
    override func drawRect(dirtyRect: NSRect) {
        
        let roundRect = NSBezierPath()
        roundRect.appendBezierPathWithRoundedRect(
            frame,
            xRadius: NSHeight(dirtyRect) / 8,
            yRadius: NSHeight(dirtyRect) / 8 )
        
        
        backColor.colorWithAlphaComponent(0.70).set()
        roundRect.fill()
        
        super.drawRect(frame)
    }
        
}
