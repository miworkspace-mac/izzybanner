//
//  NoDoz.h
//  Izzy Banner
//
//  Created by Jim Zajkowski on 7/14/15.
//  Copyright © 2015 University of Michigan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarbonHelpers : NSObject

- (void)wakeUpScreen;
- (void)allowScreenSleep;
- (void)hideMenubar;

@end
