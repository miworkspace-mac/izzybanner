//
//  LargeTextEphemeralPanel.swift
//  Izzy Banner
//
//  Created by Jim Zajkowski on 7/10/15.
//  Copyright © 2015 University of Michigan. All rights reserved.
//

import Cocoa

class LargeTextEphemeralPanel: NSWindow {

    override var canBecomeKeyWindow: Bool {
        get {
            return true
        }
    }
    
    override func keyDown(theEvent: NSEvent) {
        self.close()
    }
    
    override func mouseDown(theEvent: NSEvent) {
        self.close()
    }
    
    override func resignKeyWindow() {
        super.resignKeyWindow()

        if (self.visible)
        {
            self.close()
        }
    }

}
