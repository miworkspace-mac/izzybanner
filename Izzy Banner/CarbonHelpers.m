//
//  NoDoz.m
//  Izzy Banner
//
//  Created by Jim Zajkowski on 7/14/15.
//  Copyright © 2015 University of Michigan. All rights reserved.
//

#import "CarbonHelpers.h"
#import <Carbon/Carbon.h>
#import <IOKit/IOKitLib.h>
#import <IOKit/pwr_mgt/IOPMLib.h>

@interface CarbonHelpers()
@property (assign) IOPMAssertionID assertionId;
@end

@implementation CarbonHelpers

- (instancetype)init
{
    self = [super init];
    self.assertionId = 0;
    return self;
}

- (void)wakeUpScreen
{
    IOReturn returnValue;
    IOPMAssertionID assertion;
    IOPMAssertionID assertion2;
    
    returnValue = IOPMAssertionCreateWithName(kIOPMAssertionTypePreventUserIdleDisplaySleep, kIOPMAssertionLevelOn, CFSTR("IzzyBanner"), &assertion);
    IOPMAssertionDeclareUserActivity(CFSTR("Woken by IzzyBanner"), kIOPMUserActiveLocal, &assertion2);
    
    if (returnValue == kIOReturnSuccess)
    {
        self.assertionId = assertion;
    }
    else
    {
        self.assertionId = 0;
    }
}

- (void)allowScreenSleep
{
    if (self.assertionId != 0) {
        IOPMAssertionRelease(self.assertionId);
        self.assertionId = 0;
    }
}

- (void)hideMenubar
{
    SetSystemUIMode(kUIModeAllHidden, 0);
}


@end
